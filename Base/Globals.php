<?php

namespace Base;

class Globals {
	/**
	 * Global fields
	 * @var array $fields
	 */
	public static $fields = [];

	/**
	 * @var \PDO $dbConnection
	 */
	public static $dbConnection;
}
