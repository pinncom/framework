<?php

require_once __DIR__ . '/Autoloader.php';
require_once __DIR__ . '/../vendor/autoload.php';

\Base\Autoloader::initAutoloader();
Twig_Autoloader::register();
