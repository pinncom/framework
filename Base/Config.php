<?php

namespace Base;
/**
 * Config class
 * To access to config fields @see \Base\Config::$config
 */
class Config {
	/**
	 * Config
	 * @var array $config
	 */
	public static $config = [
		'db' => [
			'driver'   => 'mysql',
			'host'     => '127.0.0.1',
			'name'	   => 'testdb',
			'user'     => 'root',
			'password' => '1234',
		]
	];
}
