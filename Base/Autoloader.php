<?php

namespace Base;

class Autoloader {

    /**
     * Loads finds and loads php class
     *
     * @param string $className
     */
    public static function loadClass($className) {
	$path = self::parsePath($className);
        $className = array_pop($path);
        $path = implode('/', $path);
        $fileName = $path . '/' . $className . '.php';
        if (file_exists($fileName)) {
            require_once $fileName;
        }
    }

    /**
     * Parses path of class
     *
     * @param string $className
     * @return array
     */
    public static function parsePath($className)  {
        $classPath = explode('\\', $className);
        return $classPath;
    }

    /**
     *  Initialize autoloader
     */
    public static function initAutoloader() {
        spl_autoload_register('\Base\Autoloader::loadClass');
    }
}

