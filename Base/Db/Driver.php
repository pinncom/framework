<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 01.08.16
 * Time: 15:48
 */

namespace Base\Db;


class Driver
{
    /**
     * Array contains links to DB-Drivers
     *
     * @var array $_drivers
     */
    private static $_drivers = [
        'mysql' => \Base\Db\Mysql::class
    ];

    /**
     * Gets DB driver
     *
     * @param $driver
     * @return mixed
     */
    public static function get($driver) {

        if (self::$_drivers[$driver]) {
            return new self::$_drivers[$driver]();
        }
        return null;
    }

}