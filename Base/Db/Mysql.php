<?php
/**
 * Created by PhpStorm.
 * User: nihirash
 * Date: 01.08.16
 * Time: 15:43
 */

namespace Base\Db;


use Base\Config;
use Base\Globals;

class Mysql
{
    /**
     * @var string $_tableName
     */
    private $_tableName;
    /**
     * @var string $_request
     */
    private $_request;
    /**
     * @var array|string @_fields
     */
    private $_fields;

    /**
     * @var string $_order
     */
    private $_order = null;
    /**
     * @var array $_where
     */
    private $_where = null;

    /**
     * @var array $_limit
     */
    private $_limit = null;

    /**
     * @var $_mode
     */
    private $_mode;

    /**
     * Data Base
     */
    const MODE_RAW = 0;
    const MODE_SELECT = 1;

    /**
     * SQL Ordering
     */
    const ORDER_ASC = 'ASC';
    const ORDER_DESC = 'DESC';

    /**
     * Initialize DataBase connection
     */
    private function initDb() {
        $dbConfig = Config::$config['db'];

        Globals::$dbConnection = new \PDO('mysql:host='.$dbConfig['host'].
            ';dbname='.$dbConfig['name'],
            $dbConfig['user'],
            $dbConfig['password']);
    }

    /**
     * Dao constructor
     */
    public function __construct(){
        if (empty(Globals::$dbConnection)) {
            $this->initDb();
        }
    }

    /**
     * Makes RAW SQL-Query
     *
     * @param $request
     * @return mixed
     */
    public function rawSql($request) {
        $this->_mode = self::MODE_RAW;
        $this->_request = $request;
    }

    /**
     * Fetchs data from DB
     *
     * @return mixed
     */
    public function fetch() {
        $this->prepareRequest();
        $result = Globals::$dbConnection->query($this->_request)->fetchAll(\PDO::FETCH_ASSOC);

        return $result;
    }

    /**
     * Executes RAW-sql(need if it isn't SELECT)
     *
     * @return mixed
     */
    public function execute(){
        $this->prepareRequest();
        return Globals::$dbConnection->exec($this->_request);
    }

    /**
     * Prepares request
     */
    public function prepareRequest() {
        if ($this->_mode == self::MODE_SELECT) {
            $this->_request = "SELECT ";

            foreach ($this->_fields as &$field) {
                $field =  $this->qi($field);
            }

            $this->_request .= implode(',' , $this->_fields);
            $this->_request .= ' FROM ';
            $this->_request .= $this->qi($this->_tableName);

            if ($this->_where) {
                $this->_request .= ' WHERE ';
                $this->_request .= $this->_where;
            }

            if ($this->_order) {
                $this->_request .= ' ORDER BY ';
                $this->_request .= $this->_order;
            }

            if ($this->_limit) {
                $this->_request .= ' LIMIT ' . $this->_limit['offset'] . ', ' . $this->_limit['size'];
            }
        }
    }

    /**
     * Preparing for SELECT-query
     *
     * @param string $fields
     * @param bool $tableName
     * @return $this
     */
    public function select($fields = '*', $tableName = false) {
        if ($tableName !== false) {
            $this->_tableName = $tableName;
        }

        if (!is_array($fields)) {
            $fields = [$fields];
        }

        $this->_fields = $fields;
        $this->_mode = self::MODE_SELECT;
        return $this;
    }

    /**
     * WHERE Helper for SELECT Query
     *
     * @param null|array|string $cause
     * @return $this
     */
    public function where($cause = null) {
        if (!is_array($cause)) {
            $this->_where = $cause;
        } else {
            $result = "";
            foreach ($cause as $name => $value) {
                $result[] = $this->qi($name) . '=' . $this->qq($value) . ' ';
            }
            $this->_where = implode(' AND ' , $result);
        }
        return $this;
    }

    /**
     * LIMIT Helper for SELECT Query
     *
     * @param $size
     * @param int $offset
     * @return $this
     */
    public function limit($size, $offset = 0) {

        $this->_limit = [
            'size' => intval($size),
            'offset' => intval($offset)
        ];

        return $this;
    }

    /**
     * LIMIT Helper for SELECT Query(offset)
     *
     * @param $offset
     * @return $this
     */
    public function offset($offset) {
        $this->_limit['offset'] = intval($offset);
        return $this;
    }

    /**
     * ORDER helper for SELECT Query
     *
     * @param $config
     * @return $this
     */
    public function order($config) {

        if (!is_array($config)) {
            $this->_order .= $config;
            return $this;
        }

        $result = [];
        foreach ($config as $key => $mode ){
            $result[] = $this->qi($key) . ' ' . $mode;
        }

        $this->_order = implode(', ', $result);

        return $this;

    }

    /**
     * Inserts record into table
     *
     * @param array $bind
     * @param bool|string $table
     */
    public function insert($bind = [], $table = false) {

        $this->_mode = self::MODE_RAW;

        if ($table === false) {
            $table = $this->_tableName;
        }

        $this->_request = 'INSERT INTO ';
        $this->_request.= $this->qi($table);
        $values = [];
        $fields = [];
        foreach ($bind as $field => $value) {
            $values[] = $value;
            $fields[] = $field;
        }
        $this->_request.= '(' . implode(',', $fields)  . ') VALUES ';
        $this->_request.= '(' . implode(',', $values ) . ')';

    }

    /**
     * Quote SQL-string
     *
     * @param string $string
     * @param int $type
     * @return string
     */
    public function qq($string, $type = \PDO::PARAM_STR) {
        return Globals::$dbConnection->quote($string, $type);
    }

    /**
     * Quote SQL-identy
     * @param $field
     * @return string
     */
    public function qi($field) {
        return "`".str_replace("`","``",$field)."`";
    }

}