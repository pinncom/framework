<?php

namespace Base\Responser;

interface Base { 
	/**
	 * Loads template
	 * @param string $template
	 */
	public function load($template);

	/**
	 * Render template to variable
	 *
	 * @param array $variables
	 * @return string
	 */
	public function render($variables = []);

	/**
	 * Assign variables to view
	 * @param array $variables
	 */
	public function assign($variables = []);	

	/**
	 * Make response 
 	 */
	public function response();

	/**
	 * Set response code
	 *
	 * @param $code
	 * @return mixed
	 */
	public function setResponseCode($code);
}
