<?php

namespace Base\Responser;

class Twig implements Base {

	/**
	 * @var \Twig_Loader_Filesystem $_loader
	 */
	private $_loader;
 	/**
	 * @var \Twig_Environment $_twig
	 */	
	private $_twig;
	/**
	 * @var array $_vars
	 */
	private $_vars = [];

	/**
	 * Response code
	 *
	 * @var int $_responseCode
	 */
	private $_responseCode = 200;
	/**
	 * @var \Twig_TemplateInterface $_template
	 */
	private $_template;

	/**
	 * Initialize Responser
	 *
	 * @var string $path
	 */
	public function __construct($path = null) {
		if ($path===null) {
			$documentRoot = $_SERVER['DOCUMENT_ROOT'] . "/Templates";
		} else {
			$documentRoot = $path;
		}
		$this->_loader = new \Twig_Loader_Filesystem($documentRoot);
		$this->_twig   = new \Twig_Environment($this->_loader, [
			'cache' => $_SERVER['DOCUMENT_ROOT'] . "/Cache/"
		]);	
	}
	
	/**
	 * Loads template
	 * @var string $template
	 */
	public function load($template) {
		$this->_template = $this->_twig->loadTemplate($template);
	}

	/**
	 * Render template
	 *
	 * @param $variables
	 * @return string
	 */
	public function render($variables = []) {

		if ($variables===[]) {
			$variables = $this->_vars;
		}

		return $this->_template->render($variables);
	}

	/**
	 * Assign variables to view
	 * @param array $variables
	 */
	public function assign($variables = [])
	{
		$this->_vars = array_merge($this->_vars, $variables);
	}

	/**
	 * Make response
	 */
	public function response()
	{
		http_response_code($this->_responseCode);
		echo $this->render();
	}

	/**
	 * Set response code
	 *
	 * @param $code
	 * @return mixed
	 */
	public function setResponseCode($code)
	{
		$this->_responseCode = $code;
	}
}
