<?php

namespace Base\Responser;

class Json implements Base {
	/**
	 * @var array $_vars
	 */
	private $_vars = [];
	/**
	 * @var int $_responseCode
	 */
	private $_responseCode = 200;

	/**
	 * Dummy method
	 */
	public function load($template) {	
	}

	/**
	 * Render template
	 *
	 * @param $variables
	 * @return string
	 */
	public function render($variables = []) {
		if ($variables===[]) {
			$variables = $this->_vars;
		}
		return json_encode($variables);
	}

	/**
	 * Assign variables to view
	 * @param array $variables
	 */
	public function assign($variables = [])
	{
		$this->_vars = array_merge($this->_vars, $variables);
	}

	/**
	 * Make response
	 */
	public function response()
	{
		http_response_code($this->_responseCode);
		echo $this->render();
	}

	/**
	 * Set response code
	 *
	 * @param $code
	 * @return mixed
	 */
	public function setResponseCode($code)
	{
		$this->_responseCode = $code;
	}
}
