<?php

namespace Base;

/**
 * Base DAO class
 */
abstract class Dao {
	/**
	 * @var \Base\Db\Mysql $_db
	 */
	protected $_db;

	public function __construct() {
		$this->_db = Db\Driver::get(Config::$config['db']['driver']);
	}

}
