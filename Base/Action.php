<?php

namespace Base;

abstract class Action {
	/**
	 * @var \Base\Responser\Base $_responser
	 */
	protected $_responser;
	/**
  	 * Returns param for request
	 * @param string $name
	 *
	 * @return mixed|bool
 	 */
	protected function p($name) {
		if (!empty($_GET[$name])) {
			return $_GET[$name];
		}

		if (!empty($_POST[$name])) {
			return $_POST[$name];
		}

		return false;
	}

	/**
	 * Action constructor.
     */
	public function __construct(){
		if ($this->p('_ajax')) {
			$this->_responser = new Responser\Json();
		} else {
			$this->_responser = new Responser\Twig();
		}
	}
}
