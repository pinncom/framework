<?php

namespace App\Action;
use App\Dao\Example;
use Base\Action;

class Index extends Action {

	public function get() {
			$dao = new Example();
			$this->_responser->load('index.html');

			$this->_responser->assign ([
				'list' => $dao->getList()
			]);

			$this->_responser->response();
	}
}
