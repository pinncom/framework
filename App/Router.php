<?php
// Routing

/**
 * @var \Bramus\Router\Router
 */
$router = new \Bramus\Router\Router();
$router->get('/','\\App\\Action\\Index@get');
$router->run();
